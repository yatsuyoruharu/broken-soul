/// POSITION ///

x = MouseTarget ? mouse_x : Character.x;
y = MouseTarget ? mouse_y : Character.y - Character.sprite_height / 3;

/// CHANGE TARGET ///

if (Object_Mouse.ButtonReleased and MouseTarget){
	if (place_meeting(x, y, Object_Character)){
		TargettedCharacter = instance_place(x, y, Object_Character);
	}
	if (TargettedCharacter != -1){
		if (TargettedCharacter.Enemy){
			Character.Target = TargettedCharacter;
			instance_destroy();
		}
	}
}

/// CONTROL ///

if (Character.IsDying or Object_Mouse.ButtonReleased){
	instance_destroy();	
}