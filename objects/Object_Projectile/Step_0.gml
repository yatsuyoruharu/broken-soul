/// VARIABLES ///

var CheckTargetExists = false;
var CheckCollision = false;
var CollisionCharacter = -1;
var CheckIfPlayer = false;

/// CONTROL ///

CheckCollision = place_meeting(x, y, Object_Character);

if (CheckCollision)
	CollisionCharacter = instance_place(x, y, Object_Character);

if (CollisionCharacter)
	CheckIfPlayer = CollisionCharacter.Player ? true : false;
if (CheckIfPlayer)
	Target = CollisionCharacter.Player ? CollisionCharacter : -1;

CheckTargetExists = instance_exists(Target);

image_angle += 2.5;

/// BATTLE CODE ///

if (CheckTargetExists){
	if (Target != -1){
		Script_AttackSpiritual(true);	
		instance_destroy();
	}
}

x += lengthdir_x(Speed, Angle);
y += lengthdir_y(Speed, Angle);
MaxTravel -= Speed;

if (MaxTravel <= 0){
	instance_destroy();	
}