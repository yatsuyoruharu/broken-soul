Target = 0;

Player = false;
Enemy = false;

Name = "";

Strength = 0;
Spirit = 0;
Agility = 0;

TotalElan = 0;
Elan = 0;
TotalAura = 0;
Aura = 0;
Power = 0;
Ki = 0;
Resistence = 0;
Accuracy = 0;
Evasion = 0;
Lethality = 0;
Recharge = 0;
Range = 0;
AuxRange = 0;
Speed = 0;
AttackSpeed = 0;
Melee = false;
Spiritual = false;

IsMoving = false;
IsAttacking = false;
IsDying = false;
IsInRange = false;

RecoilDistance = room_speed/1.5;

image_speed = 0;
depth = -y;

HorizonalRange = 3;
VerticalRange = 1.5;
ImageSpeed = 3;

Projectile = -1;

SPYield = 0;

MyParty = 0;
MyIndex = -1;

alarm[0] = -1;
alarm[1] = -1;
AttackCountDown = 0;