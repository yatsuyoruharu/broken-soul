var Offset = Player ? -200 : 200;

draw_text(x + Offset, y - 98, "Index: " + string(MyIndex));
draw_text(x + Offset, y - 82, "Name: " + string(Name));
draw_text(x + Offset, y - 64, "Elan: " + string(TotalElan) + " / " + string(Elan));
draw_text(x + Offset, y - 48, "Aura: " + string(TotalAura) + " / " + string(Aura));
draw_text(x + Offset, y - 32, "Power: " + string(Power));
draw_text(x + Offset, y - 16, "Ki: " + string(Ki));
draw_text(x + Offset, y - 00, "Resistence: " + string(Resistence));
draw_text(x + Offset, y + 16, "Accuracy: " + string(Accuracy) + "%");
draw_text(x + Offset, y + 32, "Evasion: " + string(Evasion) + "%");
draw_text(x + Offset, y + 48, "Lethality: " + string(Lethality) + "%");
draw_text(x + Offset, y + 64, "Recharge: " + string(Recharge) + "/s");
draw_text(x + Offset, y + 82, "Soul Power: " + string(Object_System.CharacterStats[? Name][? "TotalSoulPower"]));
