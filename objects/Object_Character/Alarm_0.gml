///Execute Attack///
var CheckRange = collision_ellipse(x - Range*HorizonalRange, y - Range*VerticalRange, x + Range*HorizonalRange, y + Range*VerticalRange, Target, false, true);

image_index = 1;
AttackCountDown = Script_AttackCountDown(Agility);

if CheckRange Script_Attack();

alarm[0] = -1;
alarm[1] = AttackCountDown;