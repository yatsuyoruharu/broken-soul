/// VARIABLES ///

MyParty = Player ? Object_Player.Party : Object_Enemy.Party;
MyIndex = ds_list_find_index(MyParty, id);
PlayerParty = Object_Player.Party;
EnemyParty = Object_Enemy.Party;

var TargetExists = instance_exists(Target);
var CheckRange = collision_ellipse(x - Range*HorizonalRange, y - Range*VerticalRange, x + Range*HorizonalRange, y + Range*VerticalRange, Target, false, true);

/// CONTROL ///

Aura = Aura <= 0 ? 0 : Aura;
Elan = Elan <= 0 ? 0 : Elan;
speed = IsMoving ? Speed : 0;
if (!IsAttacking){
	IsMoving = CheckRange ? false : true;
	IsInRange = CheckRange ? true : false;
}
IsDying = Elan <= 0 ? true : false;

/// DIRECTION ///

if (TargetExists)
	image_xscale = Target.x > x ? 1 : -1;

/// AUTO TARGET ///

if (Target == 0 or !TargetExists or Target == undefined or Target.IsDying){
	Target = Script_AutoTarget();
}

/// BATTLE CODE ///

if (TargetExists and !IsDying){	
	if (IsMoving){
		image_speed = ImageSpeed/room_speed;
		move_towards_point(Target.x, Target.y, Speed);
	}
	if (IsInRange){
		image_index = image_speed > 0 ? 0 : image_index;
		image_speed = 0;
		alarm[0] = (alarm[0] == -1 && alarm[1] == -1)? AttackCountDown : alarm[0];
		IsAttacking = true;
	}
}

/// DEATH ///

if(IsDying){
	IsAttacking = false;
	IsMoving = false;
	x -= 3 * image_xscale;
	RecoilDistance --;
	if (RecoilDistance <= 0){
		if (Enemy) Script_SPCalc(id);
		Script_DeleteFromBattle();
	}
}

/// CHANGE SPRITE ///

sprite_index = Script_ChangeSprite();