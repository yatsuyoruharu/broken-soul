x = mouse_x;
y = mouse_y;
var LevelButton = -1;

ButtonReleased = mouse_check_button_released(mb_left) ? true : false;
ButtonPressed = mouse_check_button_pressed(mb_left) ? true : false;
ButtonHold = mouse_check_button(mb_left) ? true : false;

if (room_get_name(room) == "Room_StartScreen"){
	if mouse_check_button_released(mb_left){
		room_goto_next();	
	}
}

if (room_get_name(room) == "Room_MainMenu"){
	if mouse_check_button_released(mb_left){
		if (place_meeting(x, y, Object_CombatButton)){
			room_goto(Room_LevelSelect);	
		}
		if (place_meeting(x, y, Object_PartyButton)){
			instance_deactivate_object(Object_Buttons);
			instance_create_depth(400, room_height / 2, -1, Object_PartyFormation);
			instance_activate_object(Object_Back);
			Object_Back.Window = "PartyFormation";
		}
		if (place_meeting(x, y, Object_RemovePartyMember)){
			var RemoveButton = instance_place(x, y, Object_RemovePartyMember);
			ds_list_delete(Object_System.PlayerPartyFormation, RemoveButton.Index);	
		}
		if (place_meeting(x, y, Object_Back)){
			switch(Object_Back.Window){
				case "PartyFormation":
					instance_destroy(Object_PartyFormation);
					instance_activate_object(Object_Buttons);
					Object_Back.Window = "";
					instance_deactivate_object(Object_Back);
					break;
			}
		}
	}
	if mouse_check_button(mb_left and room == Room_MainMenu){
		if (place_meeting(x, y, Object_PartyFormationPortrait) and SelectedCharacter == -1){
			if (instance_place(x, y, Object_PartyFormationPortrait).Show and !instance_place(x, y, Object_PartyFormationPortrait).InParty)
				SelectedCharacter = instance_place(x, y, Object_PartyFormationPortrait);
		}
		if (SelectedCharacter != -1){
			SelectedCharacter.image_index = 1;
		}
	}else{
		if mouse_check_button_released(mb_left) and SelectedCharacter != -1{
			with(Object_PartyFormation){
				Script_CheckParty(other.Party, PlaceOne, x, y, 0);
				Script_CheckParty(other.Party, PlaceTwo, x, y, 1);
				Script_CheckParty(other.Party, PlaceThree, x, y, 2);
				Script_CheckParty(other.Party, PlaceFour, x, y, 3);
				Script_CheckParty(other.Party, PlaceFive, x, y, 4);
			}
		}
		SelectedCharacter.image_index = 0;
		SelectedCharacter = -1;	
	}
}

if (room_get_name(room) == "Room_LevelSelect"){
	if mouse_check_button_released(mb_left){
		if (place_meeting(x, y, Object_ButtonBack)){
			room_goto(Room_MainMenu);	
		}
		if (place_meeting(x, y, Object_LevelButton) and ds_list_size(Object_System.PlayerPartyFormation) > 0){
			LevelButton = instance_place(x, y, Object_LevelButton);
			Object_System.CurrentLevel = LevelButton.RoomName;
			Object_System.CurrentStageIndex = LevelButton.StageIndex;
			room_goto(asset_get_index(string("Room_Stage" + LevelButton.RoomName)));	
		}
	}
}

/// CHANGE TARGET IN BATTLE ///

if (place_meeting(x, y, Object_Character) and mouse_check_button(mb_left)){
	if (!instance_exists(Object_SelectCircle) and instance_place(x, y, Object_Character).Player){
		var ClickedCharacter = instance_place(x, y, Object_Character);
		var TargetSelection = instance_create_depth(x, y, depth - 1, Object_SelectCircle);
		TargetSelection.Character = ClickedCharacter;
		TargetSelection = instance_create_depth(x, y, depth - 2, Object_SelectCircle);
		TargetSelection.Character = ClickedCharacter;
		TargetSelection.MouseTarget = true;
	}
}