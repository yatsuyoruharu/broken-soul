if (!Script_EqualListsCheck(Party, Object_System.PlayerPartyFormation)){
	instance_destroy(Object_PartyFormationPortrait);
	instance_destroy(Object_RemovePartyMember);
	ds_list_copy(Party, Object_System.PlayerPartyFormation);
	Script_PartyFormationShow(Party);
	Script_ShowAvailablePortraits(PlayableCharacters);
}