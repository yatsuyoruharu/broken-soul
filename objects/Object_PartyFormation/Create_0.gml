Party = ds_list_create();
ds_list_copy(Party, Object_System.PlayerPartyFormation);
Script_PartyFormationShow(Party);
AvailableCharacters = ds_list_create();
PlayableCharacters = Object_System.CharacterStats;

var Character = ds_map_find_first(PlayableCharacters);

for (var i = 0; i < ds_map_size(PlayableCharacters); i ++){
	if (PlayableCharacters[? Character][? "Available"]){
		ds_list_add(AvailableCharacters, PlayableCharacters[? Character][? "Name"]);
	}
	Character = ds_map_find_next(PlayableCharacters, Character);
}

Script_ShowAvailablePortraits();

PlaceOne = [255, 65, 425, 145];
PlaceTwo = [255, 180, 425, 260];
PlaceThree = [30, 15, 200, 95];
PlaceFour = [30, 130, 200, 210];
PlaceFive = [30, 245, 200, 325];