if keyboard_check_released(vk_f1) and keyboard_check(vk_alt)
	room_restart();
	
if (InBattle){
	if (ds_list_size(Object_Player.Party) <= 0 or ds_list_size(Object_Enemy.Party) <= 0){
		InBattle = false;
		if (ds_list_size(Object_Enemy.Party) <= 0){
			var ListNextIndex = CurrentStageIndex + 1;
			if (StageList[| ListNextIndex] != undefined){
				StageList[| ListNextIndex][? "Available"] = true;
			}
		}
		room_goto(Room_MainMenu);
	}
}