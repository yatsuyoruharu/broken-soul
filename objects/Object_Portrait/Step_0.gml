if (instance_exists(Character)){
	Elan = Character.Elan;
	Aura = Character.Aura;
	if (Elan <= 0){
		image_index = 1;
		Elan = 0;
	}
	if (Aura <= 0){
		Aura = 0;
	}
	ElanPercentage = Elan/TotalElan;
	ElanBarLength = (sprite_width - 45)*ElanPercentage;
	AuraPercentage = Aura/TotalAura;
	AuraBarLength = (sprite_width - 45)*AuraPercentage;
}