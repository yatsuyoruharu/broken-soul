function Script_AttackMelee(){
	var HitChance = 0;
	var IsHit = false;
	var IsLethal = false;
	var LethalMultiplier = 1;
	var FinalDamage = 0;
	var EVA = Target.Evasion;
	var RES = Target.Resistence;
	
	HitChance = (Accuracy - EVA) <= 1 ? 1 : (Accuracy - EVA);
	HitChance = (Accuracy - EVA) >= 99 ? 99 : (Accuracy - EVA);
	IsLethal = irandom(100) <= (Lethality - EVA / 50) ? true : false;
	IsHit = irandom(100) <= HitChance ? true : false;
	
	if (IsLethal)
		LethalMultiplier = irandom(2) * 0.5 + 1.5;
	
	if (IsHit){
		FinalDamage = ((Power * 0.75) + (Power * random(0.5)) - RES) * LethalMultiplier;
		FinalDamage = floor(FinalDamage);
		FinalDamage = FinalDamage <= 1 ? 1 : FinalDamage;
		Target.Elan -= FinalDamage;
		return 1;
	}else{
		return 0;	
	}
}