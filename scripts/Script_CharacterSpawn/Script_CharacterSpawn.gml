function Script_CharacterSpawn(Caller, Party, PartyArray){
	Character = 0;
	Position = Caller == "Player" ? 0 : 1200;
	for (var i = 0; i < ds_list_size(Party); i++){
		switch(ds_list_find_value(Party, i)[1]){
			case 0:
				Character = instance_create_depth(500 + Position, 500, -1, asset_get_index(string("Object_" + ds_list_find_value(Party, i)[0])));
				break;
			case 1:
				Character = instance_create_depth(500 + Position, 750, -1, asset_get_index(string("Object_" + ds_list_find_value(Party, i)[0])));
				break;
			case 2:
				Character = instance_create_depth(200 + Position, 350, -1, asset_get_index(string("Object_" + ds_list_find_value(Party, i)[0])));
				break;
			case 3:
				Character = instance_create_depth(200 + Position, 600, -1, asset_get_index(string("Object_" + ds_list_find_value(Party, i)[0])));
				break;
			case 4:
				Character = instance_create_depth(200 + Position, 850, -1, asset_get_index(string("Object_" + ds_list_find_value(Party, i)[0])));
				break;
		}
		Character.Player = Caller == "Player" ? true : false;
		Character.Enemy = Caller == "Enemy" ? true : false;
		if Character.Player	Script_AssignStats(Character, Character.Name, "Player");
		if Character.Enemy	Script_AssignStats(Character, Character.Name, "Enemy");
		ds_list_add(PartyArray, Character);
		Character = 0;
	}
}