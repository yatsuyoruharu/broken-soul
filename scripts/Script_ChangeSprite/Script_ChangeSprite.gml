function Script_ChangeSprite(){
	var Sprite = 0;
	if (IsMoving){
		Sprite = asset_get_index(string("Sprite_" + Name + "Move"));
	}else if (IsAttacking){
		Sprite = asset_get_index(string("Sprite_" + Name + "Attack"));
	}else if (IsDying){
		Sprite = asset_get_index(string("Sprite_" + Name + "Recoil"));
	}
	return Sprite;
}