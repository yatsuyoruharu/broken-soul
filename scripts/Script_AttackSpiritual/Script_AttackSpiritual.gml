function Script_AttackSpiritual(IsProjectile){
	var YOffSet = sprite_height / 2;
	
	if (IsProjectile){
		var FinalDamage = 0;
		var DamageDifference = 0;
		
		FinalDamage =  floor((0.75 * Ki + random(0.5) * Ki));
		if (Target.Aura >= FinalDamage){
			Target.Aura -= FinalDamage;
		}else{
			DamageDifference = FinalDamage - Target.Aura;
			Target.Aura -= FinalDamage;
			Target.Elan -= DamageDifference;
		}	
	}else{
		Projectile = instance_create_depth(x, y - YOffSet, depth, asset_get_index(string("Object_" + Name + "Projectile")));	
		Script_AssignProjectileData(Projectile, id);
	}
}