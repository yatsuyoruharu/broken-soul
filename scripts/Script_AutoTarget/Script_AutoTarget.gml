function Script_AutoTarget(){
	var TargetArray = Player ? EnemyParty : PlayerParty;
	
	NearestTarget = ds_list_find_value(TargetArray, 0);
	for (var i = 0; i < ds_list_size(TargetArray); i++){
		if NearestTarget.IsDying continue;
		if (distance_to_object(ds_list_find_value(TargetArray, i)) < distance_to_object(NearestTarget)){
			NearestTarget = ds_list_find_value(TargetArray, i);	
		}
	}
	return NearestTarget;
}