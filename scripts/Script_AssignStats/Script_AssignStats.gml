function Script_AssignStats(Character, CharacterName, Caller){
	var Stats = Object_System.CharacterStats[? CharacterName];
	var Base = Object_System.StatsBase;
	var Multipliers = Object_System.StatsMultipliers;
	var	Level = global.EnemyLevel;
	if (Caller == "Player"){
		var STR = Stats[? "TotalStrength"];
		var AGI = Stats[? "TotalAgility"];
		var SPT = Stats[? "TotalSpirit"];
	}
	if (Caller == "Enemy"){
		var STR = Stats[? "BaseStrength"] + floor((irandom(Level) + Level)/2);
		var AGI = Stats[? "BaseAgility"] + floor((irandom(Level) + Level)/2);
		var SPT = Stats[? "BaseSpirit"] + floor((irandom(Level) + Level)/2);
		Character.SPYield = floor(Stats[? "SPYield"]*(1 + 0.1*Level));
	}
	with(Character){
		Strength = STR;
		Spirit = SPT;
		Agility = AGI;
		TotalElan = floor(Base[? "Elan"] +  Multipliers[? "Elan"][Stats[? "GrowthElan"]] * STR);
		Elan = TotalElan;
		TotalAura = floor(Base[? "Aura"] +  Multipliers[? "Aura"][Stats[? "GrowthAura"]] * SPT);
		Aura = TotalAura;
		Power = floor(Base[? "Power"] +  Multipliers[? "Power"][Stats[? "GrowthPower"]] * STR);
		Ki = floor(Base[? "Ki"] +  Multipliers[? "Ki"][Stats[? "GrowthKi"]] * SPT);
		Resistence = floor(Base[? "Resistence"] +  Multipliers[? "Resistence"][Stats[? "GrowthResistence"]] * STR);
		Accuracy = floor(Base[? "Accuracy"] +  Multipliers[? "Accuracy"][Stats[? "GrowthAccuracy"]] * AGI);
		Evasion = floor(Base[? "Evasion"] +  Multipliers[? "Evasion"][Stats[? "GrowthEvasion"]] * AGI);
		Lethality = floor(Base[? "Lethality"] +  Multipliers[? "Lethality"][Stats[? "GrowthLethality"]] * AGI);
		Recharge = floor(Base[? "Recharge"] +  Multipliers[? "Recharge"][Stats[? "GrowthRecharge"]] * SPT);
		Range = Stats[? "Range"];
		AuxRange = Stats[? "Range"];
		Speed = Stats[? "Speed"];
		AttackSpeed = Stats[? "AttackSpeed"];
		Melee = Stats[? "Type"] == "Melee" ? true : false;
		Spiritual = Stats[? "Type"] == "Spiritual" ? true : false;
		AttackCountDown = Script_AttackCountDown(AGI);
	}
}