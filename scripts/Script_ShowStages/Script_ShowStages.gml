function Script_ShowStages(){	
	var StageList = Object_System.StageList;
	var LevelButton = -1;

	for (var i = 0; i < ds_map_size(StageList); i ++){
		if (StageList[| i][? "Available"]){
			LevelButton = instance_create_depth(room_width - 300, 200 + 50 * i, -1, Object_LevelButton);
			LevelButton.image_index = i;
			LevelButton.RoomName = StageList[| i][? "Number"];
			LevelButton.StageIndex = i;
		}
	}
}