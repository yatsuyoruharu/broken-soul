function Script_EqualListsCheck(ListA, ListB){
	if (ds_list_size(ListA) != ds_list_size(ListB))
		return false;
	for(var i = 0; i < ds_list_size(ListA); i++){
		if (ListA[| i] != ListB[| i])
			return false;
	}
	return true;
}