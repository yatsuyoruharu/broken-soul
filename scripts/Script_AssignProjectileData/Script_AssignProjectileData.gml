function Script_AssignProjectileData(ProjectileInstance, CHAR){
	with (ProjectileInstance){
		Character = CHAR;
		Name = Character.Name;
		sprite_index = asset_get_index(string("Sprite_" + Name + "Projectile"));
		Ki = Character.Ki;
		Speed = Character.Speed*10;
		Angle = point_direction(x, y, Character.Target.x, Character.Target.y - Character.Target.sprite_height / 3);
		MaxTravel = Character.Range*2.5;
	}
}