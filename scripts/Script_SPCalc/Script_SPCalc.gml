function Script_SPCalc(Defeated){
	Formation = Object_System.PlayerPartyFormation;
	Characters = Object_System.CharacterStats;
	Character = -1;
	SP = 0;
	SPYield = Defeated.SPYield;
	for (var i = 0; i < ds_list_size(Formation); i++){
		Character = Characters[? Formation[| i][0]];
		
		SP = floor((SPYield) * (0.5 + 0.5 * global.EnemyLevel));
		
		Character[? "TotalSoulPower"] += SP;
		Character[? "SoulPower"] += SP;
	}
}