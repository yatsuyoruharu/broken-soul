function Script_PartyFormationShow(Party){
	var Portrait = -1;
	for (var i = 0; i < ds_list_size(Party); i++){
		switch(Party[| i][1]){
			case 0:
				Portrait = instance_create_depth(x + 340, y + 100, depth - 1, Object_PartyFormationPortrait);
				break;
			case 1:
				Portrait = instance_create_depth(x + 340, y + 220, depth - 1, Object_PartyFormationPortrait);
				break;
			case 2:
				Portrait = instance_create_depth(x + 110, y + 55, depth - 1, Object_PartyFormationPortrait);
				break;
			case 3:
				Portrait = instance_create_depth(x + 110, y + 175, depth - 1, Object_PartyFormationPortrait);
				break;
			case 4:
				Portrait = instance_create_depth(x + 110, y + 295, depth - 1, Object_PartyFormationPortrait);
				break;
		}
		with (Portrait){
			Name = other.Party[| i][0];
			Position = other.Party[| i][1];
			sprite_index = asset_get_index("Sprite_" + Name + "Portrait");
			RemoveButton = instance_create_depth(x + sprite_width / 2, y - sprite_height / 2, depth - 1, Object_RemovePartyMember);
			RemoveButton.Portrait = self;
			RemoveButton.Index = i;
		}
		Portrait = -1;
	}
}