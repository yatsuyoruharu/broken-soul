function Script_ShowAvailablePortraits(){
	PlayableCharacters = Object_System.CharacterStats;
	PlayerParty = Object_System.PlayerPartyFormation;
	CharacterListSize = ds_map_size(PlayableCharacters);
	SelectionPortrait = -1;

	var CharacterName = ds_map_find_first(PlayableCharacters);
	var AvailableCount = 0;
	for (var i = 0; i < CharacterListSize; i ++){
		if (PlayableCharacters[? CharacterName][? "Available"]){
			SelectionPortrait = instance_create_depth(128 + 256 * AvailableCount, 128, -1, Object_PartyFormationPortrait);
			SelectionPortrait.Name = PlayableCharacters[? CharacterName][? "Name"];
			SelectionPortrait.sprite_index = asset_get_index("Sprite_" + SelectionPortrait.Name + "Portrait");
			for (var j = 0; j < ds_list_size(PlayerParty); j++){
				if (SelectionPortrait.Name == PlayerParty[| j][0]){
					SelectionPortrait.image_index = 1;
					SelectionPortrait.InParty = true;
					break;
				}
			}
			SelectionPortrait.Show = true;
			AvailableCount ++;
		}
		SelectionPortrait = -1;
		CharacterName = ds_map_find_next(PlayableCharacters, CharacterName);
	}
}


/*var size = ds_map_size(inventory) ;
var key = ds_map_find_first(inventory);
for (var i = 0; i < size; i++;)
   {
   if key != "gold"
      {
      key = ds_map_find_next(inventory, key);
      }
   else break;
   }*/