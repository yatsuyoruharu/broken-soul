function Script_RoomPreparation(StageRoom){
	var EnemyStageData = Object_System.StageEnemyPartyData;
	var EnemyFormation = Object_System.EnemyPartyFormation;
	var StageNumber = string_delete(room_get_name(StageRoom), 1, 10);
	var EnemyFormationArray = EnemyStageData[? StageNumber][? "Formation"];

	global.EnemyLevel = EnemyStageData[? StageNumber][? "Level"];

	for (var i = 0; i < array_length(EnemyFormationArray); i ++){
		ds_list_add(EnemyFormation, EnemyFormationArray[i]);
	}

	instance_create_depth(0, 0, 0, Object_Player);
	instance_create_depth(0, 0, 0, Object_Enemy);

	ds_list_clear(EnemyFormation);

	Object_System.InBattle = true; 
}