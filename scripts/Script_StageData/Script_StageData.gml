function Script_StageData(Stages){
	var StageList = ds_list_create();
	/// INITIATE STAGE MAP ///
	
	StageNumber0101Data = ds_map_create();
	StageNumber0102Data = ds_map_create();
	StageNumber0103Data = ds_map_create();
	StageNumber0104Data = ds_map_create();
	StageNumber0105Data = ds_map_create();
	
	/// INPUT STAGE DATA ///
	
	StageNumber0101Data[? "Available"] = true;
	StageNumber0101Data[? "Cleared"] = false;
	StageNumber0101Data[? "Number"] = "0101";
	StageNumber0101Data[? "Level"] = 1;
	StageNumber0101Data[? "Formation"] = [["Faesy", 1]];
	
	StageNumber0102Data[? "Available"] = false;
	StageNumber0102Data[? "Cleared"] = false;
	StageNumber0102Data[? "Number"] = "0102";
	StageNumber0102Data[? "Level"] = 1;
	StageNumber0102Data[? "Formation"] = [["Faesy", 1], ["Tara", 2]];
	
	StageNumber0103Data[? "Available"] = false;
	StageNumber0103Data[? "Cleared"] = false;
	StageNumber0103Data[? "Number"] = "0103";
	StageNumber0103Data[? "Level"] = 1;
	StageNumber0103Data[? "Formation"] = [["Faesy", 0], ["Tara", 2], ["Ruru", 3], ["Ruru", 4]];
	
	StageNumber0104Data[? "Available"] = false;
	StageNumber0104Data[? "Cleared"] = false;
	StageNumber0104Data[? "Number"] = "0104";
	StageNumber0104Data[? "Level"] = 2;
	StageNumber0104Data[? "Formation"] = [["Faesy", 1], ["Ruru", 3]];
	
	StageNumber0105Data[? "Available"] = false;
	StageNumber0105Data[? "Cleared"] = false;
	StageNumber0105Data[? "Number"] = "0105";
	StageNumber0105Data[? "Level"] = 2;
	StageNumber0105Data[? "Formation"] = [["Faesy", 0], ["Tara", 1], ["Faesy", 2], ["Ruru", 3], ["Ruru", 4]];
	
	/// ADD STAGES TO LIST ///
	
	ds_map_add(Stages, "0101", StageNumber0101Data);
	ds_map_add(Stages, "0102", StageNumber0102Data);
	ds_map_add(Stages, "0103", StageNumber0103Data);
	ds_map_add(Stages, "0104", StageNumber0104Data);
	ds_map_add(Stages, "0105", StageNumber0105Data);
	
	ds_list_add(StageList, Stages[? "0101"], Stages[? "0102"], Stages[? "0103"], Stages[? "0104"], Stages[? "0105"]);
	return StageList;
}