function Script_CharacterPortraits(Party){
	Portrait = -1;
	
	switch(ds_list_size(Party)){
		case 1:
			Portrait = instance_create_depth(room_width/ 2, room_height - 150, -1, Object_Portrait);
			Portrait.Character = Party[| 0];
			Portrait.TotalElan = Portrait.Character.TotalElan;
			Portrait.Elan = Portrait.Character.Elan;
			Portrait.TotalAura = Portrait.Character.TotalAura;
			Portrait.Aura = Portrait.Character.Aura;
			Portrait.sprite_index = asset_get_index(string("Sprite_" + Portrait.Character.Name + "Portrait"));
			break;
		case 2:
			for (var i = 0; i < ds_list_size(Party); i ++){
				Portrait = instance_create_depth(room_width/ 2 - 300 + 450 * i, room_height - 150, -1, Object_Portrait);
				Portrait.Character = Party[| i];
				Portrait.TotalElan = Portrait.Character.TotalElan;
				Portrait.Elan = Portrait.Character.Elan;
				Portrait.TotalAura = Portrait.Character.TotalAura;
				Portrait.Aura = Portrait.Character.Aura;
				Portrait.sprite_index = asset_get_index(string("Sprite_" + Portrait.Character.Name + "Portrait"));
			}
			break;
		case 3:
			for (var i = 0; i < ds_list_size(Party); i ++){
				Portrait = instance_create_depth(room_width/ 2 - 300 + 300 * i, room_height - 150, -1, Object_Portrait);
				Portrait.Character = Party[| i];
				Portrait.TotalElan = Portrait.Character.TotalElan;
				Portrait.Elan = Portrait.Character.Elan;
				Portrait.TotalAura = Portrait.Character.TotalAura;
				Portrait.Aura = Portrait.Character.Aura;
				Portrait.sprite_index = asset_get_index(string("Sprite_" + Portrait.Character.Name + "Portrait"));
			}
			break;
		case 4:
			for (var i = 0; i < ds_list_size(Party); i ++){
				Portrait = instance_create_depth(room_width/ 2 - 450 + 300 * i, room_height - 150, -1, Object_Portrait);
				Portrait.Character = Party[| i];
				Portrait.TotalElan = Portrait.Character.TotalElan;
				Portrait.Elan = Portrait.Character.Elan;
				Portrait.TotalAura = Portrait.Character.TotalAura;
				Portrait.Aura = Portrait.Character.Aura;
				Portrait.sprite_index = asset_get_index(string("Sprite_" + Portrait.Character.Name + "Portrait"));
			}
			break;
		case 5:
			for (var i = 0; i < ds_list_size(Party); i ++){
				Portrait = instance_create_depth(room_width/ 2 - 600 + 300 * i, room_height - 150, -1, Object_Portrait);
				Portrait.Character = Party[| i];
				Portrait.TotalElan = Portrait.Character.TotalElan;
				Portrait.Elan = Portrait.Character.Elan;
				Portrait.TotalAura = Portrait.Character.TotalAura;
				Portrait.Aura = Portrait.Character.Aura;
				Portrait.sprite_index = asset_get_index(string("Sprite_" + Portrait.Character.Name + "Portrait"));
			}
			break;
	}
}