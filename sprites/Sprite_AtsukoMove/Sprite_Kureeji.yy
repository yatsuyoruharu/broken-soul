{
  "bboxMode": 2,
  "collisionKind": 2,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 50,
  "bbox_right": 180,
  "bbox_top": 150,
  "bbox_bottom": 280,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 230,
  "height": 300,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"34e410eb-3fba-4ddf-9d37-c30a856d9c95","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34e410eb-3fba-4ddf-9d37-c30a856d9c95","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":{"name":"5f652a5f-5807-467f-af70-a653f39925fb","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite_Kureeji","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"34e410eb-3fba-4ddf-9d37-c30a856d9c95","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8af4d1bd-b64e-4d61-92c5-62b52d282cf7","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8af4d1bd-b64e-4d61-92c5-62b52d282cf7","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":{"name":"5f652a5f-5807-467f-af70-a653f39925fb","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite_Kureeji","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"8af4d1bd-b64e-4d61-92c5-62b52d282cf7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"34d8190a-b10d-4d2f-b9f7-ec64a4342483","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34d8190a-b10d-4d2f-b9f7-ec64a4342483","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":{"name":"5f652a5f-5807-467f-af70-a653f39925fb","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite_Kureeji","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"34d8190a-b10d-4d2f-b9f7-ec64a4342483","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"85395bc6-9b1a-4905-94fb-e85dbe6e2eca","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"85395bc6-9b1a-4905-94fb-e85dbe6e2eca","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":{"name":"5f652a5f-5807-467f-af70-a653f39925fb","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite_Kureeji","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"85395bc6-9b1a-4905-94fb-e85dbe6e2eca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8165d918-b8f3-4d7c-8b32-11cde0824f21","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8165d918-b8f3-4d7c-8b32-11cde0824f21","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"LayerId":{"name":"5f652a5f-5807-467f-af70-a653f39925fb","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite_Kureeji","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","name":"8165d918-b8f3-4d7c-8b32-11cde0824f21","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite_Kureeji","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7fc388c8-d191-4024-ba28-864d1ace4ad6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34e410eb-3fba-4ddf-9d37-c30a856d9c95","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1c428c21-e567-471f-b396-095b9855dd39","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8af4d1bd-b64e-4d61-92c5-62b52d282cf7","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"caf98d01-e0d4-4142-b97e-63a2e5f0d71a","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34d8190a-b10d-4d2f-b9f7-ec64a4342483","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2a24648d-a277-404a-ad90-a8f4acbdc8c6","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"85395bc6-9b1a-4905-94fb-e85dbe6e2eca","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e913f01b-8166-408e-b4fb-edae4efbb984","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8165d918-b8f3-4d7c-8b32-11cde0824f21","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 115,
    "yorigin": 250,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite_Kureeji","path":"sprites/Sprite_Kureeji/Sprite_Kureeji.yy",},
    "resourceVersion": "1.3",
    "name": "Sprite_Kureeji",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5f652a5f-5807-467f-af70-a653f39925fb","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Kureeji",
    "path": "folders/Characters/Kureeji.yy",
  },
  "resourceVersion": "1.0",
  "name": "Sprite_Kureeji",
  "tags": [],
  "resourceType": "GMSprite",
}